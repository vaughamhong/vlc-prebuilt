################################################################################

# save off the local path
LOCAL_PATH_TEMP := $(LOCAL_PATH)
LOCAL_PATH := $(call my-dir)

################################################################################

include $(CLEAR_VARS)
LOCAL_MODULE := vlcjni
LOCAL_SRC_FILES := libs/Release/libvlcjni.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libiomx14
LOCAL_SRC_FILES := libs/Release/libiomx.14.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libiomx13
LOCAL_SRC_FILES := libs/Release/libiomx.13.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libiomx10
LOCAL_SRC_FILES := libs/Release/libiomx.10.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libanw21
LOCAL_SRC_FILES := libs/Release/libanw.21.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libanw18
LOCAL_SRC_FILES := libs/Release/libanw.18.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libanw14
LOCAL_SRC_FILES := libs/Release/libanw.14.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libanw13
LOCAL_SRC_FILES := libs/Release/libanw.13.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libanw10
LOCAL_SRC_FILES := libs/Release/libanw.10.so
include $(PREBUILT_SHARED_LIBRARY)

################################################################################

# Restore the local path since we overwrote it when we started
LOCAL_PATH := $(LOCAL_PATH_TEMP)

################################################################################